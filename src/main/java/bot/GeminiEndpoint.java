package bot;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.ws.rs.core.Response;

public class GeminiEndpoint {
    private static ResteasyClient client = new ResteasyClientBuilder().build();
    private static String apiTarget = "https://api.gemini.com/";

    public String getSymbol(String symbol) {
        ResteasyWebTarget getSymbol = client.target(apiTarget + "v1/pubticker/" + symbol);
        Response response = getSymbol.request().get();
        return response.readEntity(String.class);
    }


    static boolean realBuy() {
        ResteasyWebTarget buy = client.target(apiTarget + "v1/order/new");
//        privateApiTarget.request().post();
        return "" == "Accepted";
    }

    static boolean realCancel() {
        ResteasyWebTarget cancel = client.target(apiTarget + "v1/order/cancel");
//        privateApiTarget.request().post();
        return "/order/status" == "Cancelled";
    }

    static boolean fakeBuy(Double usdToSpend) {
        return true;
    }

    static boolean fakeSell(Double ethToSpend) {
        return true;
    }

}
