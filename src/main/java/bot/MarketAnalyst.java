package bot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MarketAnalyst {
    private double shortSma;
    private int shortSmaLength;
    private double longSma;
    private int longSmaLength;
    private double shortEma;
    private int shortEmaLength;
    private double longEma;
    private int longEmaLength;
    private double macd;
    private double macdSignal;
    private List<Double> macdHistory = new ArrayList<>();

    public MarketAnalyst(int shortSmaLength, int longSmaLength, int shortEmaLength, int longEmaLength) {
        this.shortSmaLength = shortSmaLength;
        this.longSmaLength = longSmaLength;
        this.shortEmaLength = shortEmaLength;
        this.longEmaLength = longEmaLength;
    }

    public void calculate(List<MarketSnapshot> history) {
        calculateShortSma(history);
        calculateLongSma(history);
        calculateShortEma(history);
        calculateLongEma(history);
        calculateMacd(history);
    }

    public void findResistance(List<MarketSnapshot> history, int age, double threshold) {
        double top = -1;
        double bottom = -1;

        if (history.size() < age) {
            return;
        }
        List<MarketSnapshot> selection = history.subList(history.size() - age, history.size() - 1);

        for (MarketSnapshot ms : selection) {
            if (top < 0) {
                top = ms.getLast();
            } else if (ms.getLast() > top) {
                top = ms.getLast();
            }
            if (bottom < 0) {
                bottom = ms.getLast();
            } else if (ms.getLast() < bottom) {
                bottom = ms.getLast();
            }
        }
    }

    private void calculateShortSma(List<MarketSnapshot> history) {
        shortSma = calculateSma(history, shortSmaLength);
    }

    private void calculateLongSma(List<MarketSnapshot> history) {
        longSma = calculateSma(history, longSmaLength);
    }

    private void calculateShortEma(List<MarketSnapshot> history) {
        shortEma = calculateEma(history, shortEmaLength);
    }

    private void calculateLongEma(List<MarketSnapshot> history) {
        longEma = calculateEma(history, longEmaLength);
    }


    private void calculateMacd(List<MarketSnapshot> history) {
        if (history.size() < 27) {
            macd = 0;
            macdSignal = 0;
        } else if (history.size() < 36){
            macd = calculateEma(history, 12) - calculateEma(history, 26);
            macdHistory.add(macd);
        } else {
            macd = calculateEma(history, 12) - calculateEma(history, 26);
            macdSignal = calculateMacdEma(macdHistory, 9);
            macdHistory.add(macd);
        }
//        System.out.println(getMacdDifference());
    }

    private double calculateSma(List<MarketSnapshot> history, int smaLength) {
        int historySize = history.size();
        if (historySize <= smaLength) {
            return -1;
        }
        List<MarketSnapshot> smaHistory = history.subList(historySize - smaLength, historySize);
        return smaHistory.stream().collect(Collectors.averagingDouble(MarketSnapshot::getLast));
    }

    private double calculateEma(List<MarketSnapshot> history, int emaLength) {
        double ema = -1;
        int historySize = history.size();
        if (historySize <= emaLength) {
            return ema;
        }
        double multiplier = 2.0 / (emaLength + 1);
        List<MarketSnapshot> emaHistory = history.subList(historySize - emaLength, historySize);
        for (MarketSnapshot snapshot : emaHistory) {
            if (ema < 0) {
                ema = emaHistory.get(0).getLast();
            } else {
                double price =  snapshot.getLast();
                ema = multiplier * price + ema * (1 - multiplier);
            }
        }
        return ema;
    }

    private double calculateMacdEma(List<Double> history, int emaLength) {
        double ema = -1;
        int historySize = history.size();
        if (historySize <= emaLength) {
            return ema;
        }
        double multiplier = 2.0 / (emaLength + 1);
        List<Double> emaHistory = history.subList(historySize - emaLength, historySize);
        for (Double record : emaHistory) {
            if (ema < 0) {
                ema = emaHistory.get(0);
            } else {
                double price =  record;
                ema = multiplier * price + ema * (1 - multiplier);
            }
        }
        return ema;
    }

    public double getShortSma() {
        return shortSma;
    }

    public double getLongSma() {
        return longSma;
    }

    public double getShortEma() {
        return shortEma;
    }

    public double getLongEma() {
        return longEma;
    }

    public double getMacdDifference() {
        return macd - macdSignal;
    }
}
