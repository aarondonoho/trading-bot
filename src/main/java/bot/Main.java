package bot;

import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        Historian historian = new Historian();
        Trader trader = new Trader(historian);
        double oldLastPrice = -1;
        double lastPrice = -1;
        try {
            while (true) {
                TimeUnit.SECONDS.sleep(5);
                historian.fetch("ethusd");

                lastPrice = historian.getHistory().get(historian.getHistory().size() - 1).getLast();
                if (lastPrice != oldLastPrice) {
                    System.out.println(lastPrice);
                    oldLastPrice = lastPrice;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
