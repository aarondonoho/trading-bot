package bot;

public class Volume {
    Double ETH;
    Double USD;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    long timestamp;

    public Double getETH() {
        return ETH;
    }

    public void setETH(Double ETH) {
        this.ETH = ETH;
    }

    public Double getUsd() {
        return USD;
    }

    public void setUsd(Double usd) {
        this.USD = usd;
    }
}
