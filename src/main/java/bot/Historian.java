package bot;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;

class Historian {

    private GsonBuilder gsonBuilder = new GsonBuilder();
    private ArrayList<MarketSnapshot> history = new ArrayList<>();
    private ArrayList<Trader> subscribers = new ArrayList<>();

    public ArrayList<Trader> getSubscribers() {
        return subscribers;
    }

    public ArrayList<MarketSnapshot> getHistory() {
        return history;
    }

    public void fetch(String symbol) {
        GeminiEndpoint endpoint = new GeminiEndpoint();
        history.add(convert(endpoint.getSymbol(symbol)));
        for (Trader trader : subscribers) {
            trader.operate(history);
        }
    }

    public void subscribe(Trader trader) {
        subscribers.add(trader);
        System.out.println("subscribers: " + subscribers);
    }

    private MarketSnapshot convert(String geminiResponse) {
        return gsonBuilder.create().fromJson(geminiResponse, MarketSnapshot.class);
    }

}
