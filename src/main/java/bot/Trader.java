package bot;

import java.util.List;

public class Trader {
    private double usd = 50;
    private double eth = 0;
    private Historian historian = null;
    private MarketAnalyst analyst = null;
    private boolean inEth = false;
    private boolean belowLong = false;
    private boolean downOnShort = false;
    private boolean smaDirectionUp = false;

    Trader(Historian historian) {
        historian.subscribe(this);
        this.historian = historian;
        this.analyst = createStrategy();
    }

    public void operate(List<MarketSnapshot> history) {
        analyst.calculate(history);
        if (inEth) {
            if ((analyst.getShortEma() < analyst.getShortSma() &&
                    analyst.getShortEma() < analyst.getLongSma()
                ) || analyst.getMacdDifference() > 0.6) {
                fakeSell(eth);
            }
        } else {
            if ((analyst.getShortEma() > analyst.getShortSma() &&
                    analyst.getShortSma() > analyst.getLongSma() &&
                    analyst.getShortSma() > analyst.getLongSma() &&
                    analyst.getShortEma() > analyst.getLongEma()
                ) || analyst.getMacdDifference() < 0.6) {
                fakeBuy(usd);
            }
        }
    }

    private void fakeBuy(double usdToSpend) {
        int hs = historian.getHistory().size();
        MarketSnapshot ms = historian.getHistory().get(hs - 1);
        if (GeminiEndpoint.fakeBuy(usdToSpend)) {
            double willBuy = Math.floor(usdToSpend / ms.ask * .9975 * 1000000) / 1000000;
            eth += willBuy;
            this.usd = 0;
            inEth = true;
            System.out.println("Bought " + willBuy + " ETH @ " + ms.ask);
            System.out.println("Net worth: $" + (eth * ms.ask + usd));
        }
    }


    private void fakeSell(double ethToSell) {
        int hs = historian.getHistory().size();
        MarketSnapshot ms = historian.getHistory().get(hs - 1);
        if (GeminiEndpoint.fakeSell(ethToSell)) {
            double willSell = Math.floor(ethToSell * ms.bid * .9975 * 100) / 100;
            usd += willSell;
            this.eth = 0;
            inEth = false;
            System.out.println("Sold ETH for " + willSell + " USD @ " + ms.bid);
            System.out.println("Net worth: $" + (eth * ms.ask + usd));
        }
    }

    private MarketAnalyst createStrategy() {
        return new MarketAnalyst(15, 25, 12, 45);
    }

}
