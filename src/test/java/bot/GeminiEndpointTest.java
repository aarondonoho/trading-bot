package bot;

import org.junit.Test;

import static org.apache.commons.lang3.StringUtils.contains;
import static org.junit.Assert.assertTrue;

public class GeminiEndpointTest {
    GeminiEndpoint endpoint = new GeminiEndpoint();

    @Test
    public void symbolsIncludeEthereum() {
        String response = endpoint.getSymbol("ethusd");
        assertTrue(contains(response,"bid"));
        assertTrue(contains(response,"ask"));
        assertTrue(contains(response,"volume"));
        assertTrue(contains(response,"ETH"));
        assertTrue(contains(response,"USD"));
        assertTrue(contains(response,"timestamp"));
        assertTrue(contains(response,"last"));
    }
}