package bot;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class HistorianTest {
    private Historian historian = new Historian();

    @Test
    public void historianHasHistoryOfLastRequest() {
        historian.fetch("ethusd");
        assertTrue(historian.getHistory().get(0).volume.ETH > 0);
    }

}